import datetime
import uuid
from typing import Union, NoReturn, Optional
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import Column, Unicode, BigInteger, String, Integer
from sqlalchemy.types import BigInteger
from ..exception import PasswordDoesNotMatchException
from core.db import Base
from core.db.mixins import TimestampMixin


class UserModel(Base, TimestampMixin):
    __tablename__ = "users"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    firstname = Column(String, nullable=False, unique=True)
    lastname = Column(String, nullable=False, unique=True)
    mobile = Column(String(15), nullable=False, unique=True)
    password = Column(Unicode(255), nullable=False)
    email = Column(Unicode(255), nullable=False, unique=True)
    nickname = Column(Unicode(255), nullable=False, unique=True)

    @classmethod
    def _is_password_match(cls, password1: str, password2: str) -> bool:
        return password1 == password2

    @classmethod
    def create(
            cls,
            id: str,
            firstname: str,
            lastname: str,
            mobile: str,
            password1: str,
            password2: str,
            email: str,
            nickname: str,) -> Union["User", NoReturn]:
        if not cls._is_password_match(password1=password1, password2=password2):
            raise PasswordDoesNotMatchException

        created_at = datetime.datetime.utcnow()
        updated_at = datetime.datetime.utcnow()
        return cls(
            id=id,
            firstname=firstname,
            lastname=lastname,
            mobile=mobile,
            password=password1,
            email=email,
            nickname=nickname,
            created_at=created_at,
            updated_at=updated_at

        )

    def change_password(self, password1: str, password2: str) -> Optional[NoReturn]:
        if not self._is_password_match(password1=password1, password2=password2):
            raise PasswordDoesNotMatchException

        self.password = password1
