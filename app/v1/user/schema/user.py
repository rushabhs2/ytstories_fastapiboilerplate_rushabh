from datetime import datetime
from uuid import UUID

from pydantic import Field
from fastapi_utils.api_model import APIMessage, APIModel


class User(APIModel):
    id: str = Field(None, description="ID")
    firstname: str = Field(None, description="Firstname")
    lastname: str = Field(None, description="Lastname")
    password: str = Field(None, description="Password")
    email: str = Field(None, description="Email")
    mobile: str = Field(None, description="Mobile")
    nickname: str = Field(None, description="Nickname")
    is_admin: bool = Field(None, description="Is admin")
    created_at: datetime = Field(None, description="Create Time")
    updated_at: datetime = Field(None, description="Update Time")
