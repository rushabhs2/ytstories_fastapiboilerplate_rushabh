from datetime import datetime
from uuid import UUID

from pydantic import BaseModel


class CreateUserResponse(BaseModel):
    # id: int
    firstname: str
    lastname: str
    mobile: str
    email: str
    nickname: str
    createdAt: datetime
    updatedAt: datetime

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "id": 1,
                "firstname": "Duke",
                "lastname": "xyz",
                "mobile": "9651235478",
                "nickname": "hide",
                "email": "hide@hide.com",
                "created_at": "2021-11-11T07:50:54.289Z",
                "updated_at": "2021-11-11T07:50:54.289Z",
            }
        }


class GetUserResponse(BaseModel):
    id: str
    firstname: str
    lastname: str
    mobile: str
    nickname: str
    email: str
    createdAt: datetime
    updatedAt: datetime

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "id": 1,
                "firstname": "Duke",
                "lastname": "xyz",
                "mobile": "9651235478",
                "nickname": "hide",
                "email": "hide@hide.com",
                "created_at": "2021-11-11T07:50:54.289Z",
                "updated_at": "2021-11-11T07:50:54.289Z",
            }
        }

