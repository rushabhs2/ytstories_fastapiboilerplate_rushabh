from core.exceptions import CustomException


class PasswordDoesNotMatchException(CustomException):
    code = 401
    error_code = 20000
    message = "password does not match"


class DuplicateEmailOrNicknameException(CustomException):
    code = 400
    error_code = 20001
    message = "duplicate email or nickname"


class UserNotFoundException(CustomException):
    code = 404
    error_code = 20002
    message = "user not found"


class InvalidMobileNumberException(CustomException):
    code = 422
    error_code = 20003
    message = "Please enter valid mobile number"


class InvalidEmailException(CustomException):
    code = 422
    error_code = 20003
    message = "Please enter valid email"


class InvalidNicknameException(CustomException):
    code = 422
    error_code = 20003
    message = "Please enter valid nickname"


class InvalidNicknameException(CustomException):
    code = 422
    error_code = 20003
    message = "Please enter valid nickname"


class InvalidFirstnameException(CustomException):
    code = 422
    error_code = 20003
    message = "Please enter valid firstname"


class InvalidLastnameException(CustomException):
    code = 422
    error_code = 20003
    message = "Please enter valid lastname"
