from typing import NoReturn, Union
from uuid import uuid4

from fastapi import HTTPException
from pythondi import inject
from core.db import Transactional, Propagation
import re

from ..models import UserModel
from ..exception import (
    DuplicateEmailOrNicknameException,
    UserNotFoundException,
    InvalidMobileNumberException,
    InvalidEmailException,
    InvalidNicknameException,
    InvalidFirstnameException,
    InvalidLastnameException
)
from ..repository import UserRepo
from ..schema import User


class UserCommandService:
    @inject()
    def __init__(self, user_repo: UserRepo):
        self.user_repo = user_repo

    @Transactional(propagation=Propagation.REQUIRED)
    async def create_user(
            self, email: str, password1: str, password2: str, nickname: str
            , firstname: str, lastname: str, mobile: str) -> Union[User, NoReturn]:
        if await self.user_repo.get_by_email_or_nickname(
                email=email,
                nickname=nickname,
        ):

            raise DuplicateEmailOrNicknameException

        regex = re.compile(r'^[A-Z]')

        if not regex.search(firstname):
            raise InvalidFirstnameException

        regex = re.compile(r'^[A-Z]')

        if not regex.search(lastname):
            raise InvalidLastnameException

        if len(mobile) > 10:
            raise InvalidMobileNumberException

        regex = re.compile(r'^[6-9]\d{9}$')

        if not regex.search(mobile):
            # print(mobile)
            raise HTTPException(status_code=404, detail="Invalid format")

        regex = re.compile(r'^[a-z0-9]+[\._]?[ a-z0-9]+[@]\w+[. ]\w{2,3}$')

        if not regex.search(email):
            raise InvalidEmailException

        if len(nickname) < 8:
            raise InvalidNicknameException
        id = uuid4
        user = UserModel.create(
            id=id,
            firstname=firstname,
            lastname=lastname,
            mobile=mobile,
            password1=password1,
            password2=password2,
            email=email,
            nickname=nickname,

        )
        user = await self.user_repo.save(user=user)
        print(user.firstname)
        return User.from_orm(user)

    @Transactional(propagation=Propagation.REQUIRED)
    async def update_password(
            self,
            user_id: int,
            password1: str,
            password2: str,
    ) -> Union[User, NoReturn]:
        user = await self.user_repo.get_by_id(user_id=user_id)
        if not user:
            raise UserNotFoundException

        user.change_password(password1=password1, password2=password2)
        return User.from_orm(user)
